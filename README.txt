What does tckn_kontrol do?
---
tckn_kontrol is a module for validating TCKN (Turkish Republic Identification
Number) in the user registration block.

7.x-2.0 will be complete rewrite allowing you to have your users save their
TCKN.
